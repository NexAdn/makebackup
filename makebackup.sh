#!/bin/bash

info() {
	echo "[I] $*"
}

error() {
	echo "[E] $*" >&2
}

# TODO: Commandline to retrieve repo password
export BORG_PASSCOMMAND=""
# TODO: Repo location (e.g. host:/path/to/repo)
export BORG_REPO=""

# TODO: Backup Root dir (e.g. /home/USERNAME/)
BACKUP_ROOT=""
# TODO: subdirs/files to backup, space-separated (e.g. Pictures Documents)
BACKUP_DIRS=""
# TODO: Directory, where makebackup files are located
RES_DIR=""

# TODO: Icinga2 API Basic auth
SUBMIT_AUTH=""
# TODO: Icinga2 API Endpoint URL
SUBMIT_URL=""
# TODO: Icinga2 Host name
SUBMIT_HOST=""
# TODO: Icinga2 Service name
SUBMIT_SERVICE=""
CURL="curl -s"

submit_check_result() {
	curl_output=$($CURL -u $SUBMIT_AUTH -X POST -H "Accept: application/json" $SUBMIT_URL?service=$SUBMIT_HOST!$SUBMIT_SERVICE -d "
	{
		\"exit_status\": $1,
		\"plugin_output\": \"$2\",
		\"performance_data\": $3
	}")
	if [[ "$?" == "0" ]]; then
		info "cURL Returned: $curl_output"
	else
		error "cURL Error: $curl_output"
	fi
}

archive_name=$(date -I)

info "Creating backup"
cd $BACKUP_ROOT
borg_output=$(borg create --stats ::$archive_name $BACKUP_DIRS 2>&1)
borg_res=$?
if [[ "$borg_res" != "0" ]]; then
	error "Backup failed: $borg_output"
else
	info "$borg_output"
fi

perfdata=$(echo "$borg_output" | awk -f $RES_DIR/borg_perfdata.awk | xargs)
sanitized_output=$(echo "$borg_output" | awk -f $RES_DIR/borg_sanitize_output.awk | xargs)
echo "$sanitized_output" | grep 'Archive .* already exists' > /dev/null
grep_res=$?

if [[ "$borg_res" != "0" ]]; then
	if [[ "$grep_res" == "0" ]]; then
		info "Submitting warning"
		submit_check_result 1 "Backup already exists" "$perfdata"
		exit 1
	else
		info "Submitting error"
		submit_check_result 2 "Failed to make backup\n$sanitized_output" "$perfdata"
		exit 2
	fi
else
	info "Submitting success"
	submit_check_result 0 "Backup success" "$perfdata"
fi
