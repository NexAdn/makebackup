BEGIN {
	print "[";
}
END {
	print "]";
}
/^Number of files:/ {
	printf "\\\"num_files=";
	printf $4;
	print "\\\",";
}
/^Utilization of max. archive size:/ {
	printf "\\\"util_max_archive_size=";
	printf $6;
	print "\\\",";
}
/^Duration:/ {
	printf "\\\"duration=";
	printf $2;
	printf "s";
	print "\\\",";
}
/^This archive:/ {
	printf "\\\"this_original_size=";
	printf $3;
	printf $4;
	print "\\\",";
	printf "\\\"this_compressed_size=";
	printf $5;
	printf $6;
	print "\\\",";
	printf "\\\"this_deduplicated_size=";
	printf $7;
	printf $8;
	print "\\\",";
}
/^All archives:/ {
	printf "\\\"all_original_size=";
	printf $3;
	printf $4;
	print "\\\",";
	printf "\\\"all_compressed_size=";
	printf $5;
	printf $6;
	print "\\\",";
	printf "\\\"all_deduplicated_size=";
	printf $7;
	printf $8;
	print "\\\",";
}
/^Chunk index:/ {
	printf "\\\"unique_chunks=";
	printf $3;
	print "\\\",";
	printf "\\\"total_chunks=";
	printf $4;
	print "\\\"";
}
